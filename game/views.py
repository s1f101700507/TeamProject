from django.shortcuts import render,redirect
from .models import Game
from .models import Naiyou

# Create your views here.
def game(request):
    """一覧画面"""
    game = Game.objects.all()
    return render(request,
    'game/game.html',
    {'games':game})

    return render(request,'game/game.html')

def create(request):
    """登録画面"""
    if  request.method == 'POST':
        game = Game(
          theme=request.POST.get('theme'),
          title=request.POST.get('title')
        )
        game.save()
        return redirect('game')
    return render(request,'game/create.html')

def naiyou(request):
    """内容画面"""
    return render(request,'game/naiyou.html')

def naiyoucreate(request):
    """内容登録画面"""
    if  request.method == 'POST':
        naiyou = Naiyou(
          title=request.POST.get('title'),
          content=request.POST.get('content')
        )
        naiyou.save()
        return redirect('game/naiyo')
    return render(request,'game/naiyoucreate.html')
