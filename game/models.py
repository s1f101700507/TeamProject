from django.db import models

# Create your models here.

class Game(models.Model):
    """ゲームのモデル"""
    title = models.CharField(max_length=256)
    theme = models.CharField(max_length=256)
    content = models.CharField(max_length=256)
    created = models.TextField()

    def __str__(self):
        return self.title

class Naiyou(models.Model):
    """内容のモデル"""
    title = models.CharField(max_length=256)
    content = models.CharField(max_length=256)

    def __str__(self):
        return self.title
