from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.game, name='game'),

    url(r'^game/create', views.create, name = 'create'),

    url(r'^game/naiyou$', views.naiyou, name='naiyou'),

    url(r'^game/naiyoucreate', views.naiyoucreate, name = 'naiyoucreate'),
]
